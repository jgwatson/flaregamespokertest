This program contains a simple Poker Game. 

The requirements where a hand comparison. The class Hand contains a compareTo method which returns a String class with the result after comparing the hands.

In order to mimic a real life poker game, a class Deck is developed and contains all possible cards which are then shuffled by substituting pairs of cards in the initial array. As default there are 100 iterations, but the number of iterations can be changed.

The program implements a simple command interface called through the class PokerUI. A new deck will be created and shuffled in default 100 iterations. Two hands will be created from the shuffled deck by obtaining the first 5 cards and assigning them to the first hand, and then the next 5 cards to the second hand.

The program returns a String array with the first and second hand following the format of the cards as specified in the task (each card is represented by a String of length = 2, with first character being the value and second being the suit). Afterwards, which card won will be show.

For quality assurance, some unit tests where developed and committed here.