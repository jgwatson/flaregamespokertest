package poker.game;

import static org.junit.Assert.*;
import org.junit.Test;

public class DeckTest {
	@Test()
	public void correctNumberOfCards() {
		Deck deck = new Deck();
		assertEquals(deck.getUnShuffledCards().size(), 52);
		assertEquals(deck.getShuffledCards().size(), 52);
	}

	@Test()
	public void decksContainsAllCards() {
		String[] values = Card.VALUES;
		String[] suits = Card.SUITS;
		Deck deck = new Deck();
		for (String suit : suits) {
			for (String value : values) {
				String cardString = value + suit;
				Card card = new Card(cardString);
				assertTrue(deck.getUnShuffledCards().contains(card));
				assertTrue(deck.getShuffledCards().contains(card));
			}
		}
	}
	
	@Test()
	public void shuffledCardsAreNotSameAsUnShaffled(){
		Deck deck = new Deck();
		assertFalse(deck.getShuffledCards().equals(deck.getUnShuffledCards()));
	}

}
