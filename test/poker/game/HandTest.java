package poker.game;

import org.junit.Test;
import static org.junit.Assert.*;

public class HandTest {
	public Hand twoPairsAce() {
		Card[] cards = new Card[5];
		cards[0] = new Card("AD");
		cards[1] = new Card("AH");
		cards[2] = new Card("2H");
		cards[3] = new Card("2C");
		cards[4] = new Card("6H");
		Hand hand = new Hand(cards);
		return hand;
	}
	
	public Hand twoPairsKing() {
		Card[] cards = new Card[5];
		cards[0] = new Card("KD");
		cards[1] = new Card("KH");
		cards[2] = new Card("2H");
		cards[3] = new Card("2C");
		cards[4] = new Card("6H");
		Hand hand = new Hand(cards);
		return hand;
	}
	
	public Hand highAceOfDimonds() {
		Card[] cards = new Card[5];
		cards[0] = new Card("AD");
		cards[1] = new Card("2H");
		cards[2] = new Card("5H");
		cards[3] = new Card("4H");
		cards[4] = new Card("6H");
		Hand hand = new Hand(cards);
		return hand;
	}
	
	public Hand highAceOfCubs() {
		Card[] cards = new Card[5];
		cards[0] = new Card("AC");
		cards[1] = new Card("2H");
		cards[2] = new Card("5H");
		cards[3] = new Card("4H");
		cards[4] = new Card("6H");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand straightFlushOfHeartsWithMax6() {
		Card[] cards = new Card[5];
		cards[0] = new Card("3H");
		cards[1] = new Card("2H");
		cards[2] = new Card("5H");
		cards[3] = new Card("4H");
		cards[4] = new Card("6H");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand fourOfAKind4() {
		Card[] cards = new Card[5];
		cards[0] = new Card("4H");
		cards[1] = new Card("4D");
		cards[2] = new Card("4C");
		cards[3] = new Card("4S");
		cards[4] = new Card("AH");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand flushOfDiamondsMax10() {
		Card[] cards = new Card[5];
		cards[0] = new Card("2D");
		cards[1] = new Card("3D");
		cards[2] = new Card("5D");
		cards[3] = new Card("7D");
		cards[4] = new Card("TD");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand fullHouse() {
		Card[] cards = new Card[5];
		cards[0] = new Card("2H");
		cards[1] = new Card("2D");
		cards[2] = new Card("4H");
		cards[3] = new Card("4D");
		cards[4] = new Card("4C");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand straightMax7() {
		Card[] cards = new Card[5];
		cards[0] = new Card("6H");
		cards[1] = new Card("5D");
		cards[2] = new Card("7H");
		cards[3] = new Card("3D");
		cards[4] = new Card("4C");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand threeOfAKindMax7() {
		Card[] cards = new Card[5];
		cards[0] = new Card("7H");
		cards[1] = new Card("7D");
		cards[2] = new Card("7H");
		cards[3] = new Card("3D");
		cards[4] = new Card("4C");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand twoPairsMax3() {
		Card[] cards = new Card[5];
		cards[0] = new Card("2H");
		cards[1] = new Card("2D");
		cards[2] = new Card("3H");
		cards[3] = new Card("3D");
		cards[4] = new Card("AC");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand pairEight() {
		Card[] cards = new Card[5];
		cards[0] = new Card("8H");
		cards[1] = new Card("8D");
		cards[2] = new Card("3H");
		cards[3] = new Card("2D");
		cards[4] = new Card("AC");
		Hand hand = new Hand(cards);
		return hand;
	}

	public Hand highCardAce() {
		Card[] cards = new Card[5];
		cards[0] = new Card("8H");
		cards[1] = new Card("2D");
		cards[2] = new Card("3H");
		cards[3] = new Card("4D");
		cards[4] = new Card("AC");
		Hand hand = new Hand(cards);
		return hand;
	}

	@Test()
	public void testStraightFlushWithMax6() {
		Hand hand = straightFlushOfHeartsWithMax6();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.STRAIGHT_FLUSH);
		assertEquals(hand.getHandValue(), 4);
	}

	@Test()
	public void testFourOfAKind4() {
		Hand hand = fourOfAKind4();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.FOUR_OF_AKIND);
		assertEquals(hand.getHandValue(), 2);
	}

	@Test()
	public void testFlushOfDiamondsMax10() {
		Hand hand = flushOfDiamondsMax10();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.FLUSH);
		assertEquals(hand.getHandValue(), 8);
	}

	@Test()
	public void testFullHouse() {
		Hand hand = fullHouse();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.FULL_HOUSE);
		assertEquals(hand.getHandValue(), 2);
	}

	@Test()
	public void testStraight() {
		Hand hand = straightMax7();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.STRAIGHT);
		assertEquals(hand.getHandValue(), 5);
	}

	@Test()
	public void testThreeOfAKindMax7() {
		Hand hand = threeOfAKindMax7();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.THREE_OF_A_KIND);
		assertEquals(hand.getHandValue(), 5);
	}

	@Test()
	public void testTwoPairsMax3() {
		Hand hand = twoPairsMax3();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.TWO_PAIRS);
		assertEquals(hand.getHandValue(), 1);
	}

	@Test()
	public void testPairEight() {
		Hand hand = pairEight();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.PAIR);
		assertEquals(hand.getHandValue(), 6);
	}

	@Test()
	public void testHighCardAce() {
		Hand hand = highCardAce();
		HandRank handRank = hand.rank();
		assertEquals(handRank, HandRank.HIGH_CARD);
		assertEquals(hand.getHandValue(), 12);
	}

	@Test()
	public void testNotAFlush() {
		Hand hand = highCardAce();
		HandRank handRank = hand.rank();
		assertNotEquals(handRank, HandRank.FLUSH);
	}

	@Test()
	public void testCompareAFlushAgainstAHighAce() {
		Hand firstHand = straightFlushOfHeartsWithMax6();
		Hand secondHand =  highCardAce();
		String theRealResult = firstHand.compareTo(secondHand);
		String expectedResult = "";
		expectedResult += Hand.FIRST_HAND + HandRank.STRAIGHT_FLUSH.getCause() + System.lineSeparator();
		expectedResult += Hand.SECOND_HAND + HandRank.HIGH_CARD.getCause()  + System.lineSeparator();
		expectedResult += Hand.FIRST_HAND_WINS;
		assertEquals(theRealResult, expectedResult);
	}
	
	@Test()
	public void testCompairADrawOfAces() {
		Hand firstHand = highAceOfDimonds();
		Hand secondHand =  highAceOfCubs();
		String theRealResult = firstHand.compareTo(secondHand);
		String expectedResult = "";
		expectedResult += Hand.FIRST_HAND + HandRank.HIGH_CARD.getCause() + System.lineSeparator();
		expectedResult += Hand.SECOND_HAND + HandRank.HIGH_CARD.getCause()  + System.lineSeparator();
		expectedResult += Hand.DRAW;
		assertEquals(theRealResult, expectedResult);
	}
	
	@Test()
	public void testCompairTwoPairsHighKingVersusHighAce() {
		Hand firstHand = twoPairsAce();
		Hand secondHand =  twoPairsKing();
		String theRealResult = firstHand.compareTo(secondHand);
		String expectedResult = "";
		expectedResult += Hand.FIRST_HAND + HandRank.TWO_PAIRS.getCause() + System.lineSeparator();
		expectedResult += Hand.SECOND_HAND + HandRank.TWO_PAIRS.getCause()  + System.lineSeparator();
		expectedResult += Hand.FIRST_HAND_WINS;
		assertEquals(theRealResult, expectedResult);
	}
	
	@Test()
	public void testCompairAStraightFlushAginstFullHouse() {
		Hand firstHand = straightFlushOfHeartsWithMax6();
		Hand secondHand =  fullHouse();
		String theRealResult = firstHand.compareTo(secondHand);
		String expectedResult = "";
		expectedResult += Hand.FIRST_HAND + HandRank.STRAIGHT_FLUSH.getCause() + System.lineSeparator();
		expectedResult += Hand.SECOND_HAND + HandRank.FULL_HOUSE.getCause()  + System.lineSeparator();
		expectedResult += Hand.FIRST_HAND_WINS;
		assertEquals(theRealResult, expectedResult);
	}
}
