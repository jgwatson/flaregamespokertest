package poker.game;

import static org.junit.Assert.*;

import org.junit.Test;

public class CardTest {

	@Test(expected = RuntimeException.class)
	public void invalidCardInitializationString() {
		String valueAndSuit = "XX";
		Card card = new Card(valueAndSuit);
	}
	@Test(expected = RuntimeException.class)
	public void invalidCardInitialization() {
		Card card = new Card(-1,-1);
	}
	
	@Test(expected = RuntimeException.class)
	public void invalidCardInitializationBigFigures() {
		Card card = new Card(20,20);
	}
	
	@Test
	public void initializeAceOfHearts(){
		Card card = new Card(12,2);
		String cardStringValue = card.toString();
		assertEquals(cardStringValue,"AH");
	}
	
	@Test
	public void initializeAceOfHeartsAsString(){
		Card card = new Card("AH");
		String cardStringValue = card.toString();
		assertEquals(cardStringValue,"AH");
	}
}
