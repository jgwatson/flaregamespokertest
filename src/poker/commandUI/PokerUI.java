package poker.commandUI;

import poker.game.Hand;
import poker.game.Card;
import poker.game.Deck;


public class PokerUI {
private static String FirstHandIs = "First hand is " + System.lineSeparator();
private static String SecondHandIs = "Second Hand is " + System.lineSeparator();


	public static void main(String[] args) {
			Deck deck = new Deck();
			Hand firstHand = new Hand(deck);
			Hand secondHand = new Hand(deck);
			System.out.println(FirstHandIs);
			for (Card inFirstHand: firstHand.getCards()){
				System.out.println(inFirstHand.toString() + System.lineSeparator());
			}
			System.out.println(SecondHandIs);
			for (Card inSecondHand: secondHand.getCards()){
				System.out.println(inSecondHand.toString() + System.lineSeparator());
			}
			
			System.out.println(firstHand.compareTo(secondHand));
			
	}

	
}
