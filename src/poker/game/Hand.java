package poker.game;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Hand {

	private Card[] cards;
	private int handValue;
	public static String FIRST_HAND = "First hand best is ";
	public static String SECOND_HAND = "Second hand best is ";
	public static String FIRST_HAND_WINS = "First hand wins!";
	public static String SECOND_HAND_WINS = "Second hand wins!";
	public static String DRAW = "No hand won, it's a draw!!";

	public Hand(Card[] cards) {
		this.cards = cards;
	}

	public Hand(Deck deck) {
		cards = new Card[5];
		for (int i = 0; i < 5; i++) {
			cards[i] = deck.drawACard();
		}
	}

	public Card[] getCards() {
		return cards;
	}

	public void setCards(Card[] cards) {
		this.cards = cards;
	}
	
	public int getHandValue(){
		return handValue;
	}
	
	public void setHandValue(int value){
		handValue = value;
	}

	private Card[] sortCards() {
		List<Card> sortedList = Arrays.asList(cards.clone());
		Collections.sort(sortedList, (c1, c2) -> c1.getValue() - c2.getValue());
		return (Card[]) sortedList.toArray();
	}

	private Map<Integer, Long> countSuits(Card[] cardsToEvaluate) {
		Map<Integer, Long> map = Arrays.stream(cardsToEvaluate)
				.collect(Collectors.groupingBy(Card::getSuit, Collectors.counting()));
		return map;
	}

	private Map<Integer, Long> countValues(Card[] cardsToEvaluate) {
		Map<Integer, Long> map = Arrays.stream(cardsToEvaluate)
				.collect(Collectors.groupingBy(Card::getValue, Collectors.counting()));
		return map;
	}

	public HandRank rank() {
		HandRank handRank = null;
		Card[] sortedCards = sortCards();
		int highCard;
		boolean isFlush = false;
		boolean isStraight = false;
		boolean isThree = false;
		boolean isPair = false;
		boolean isSecondPair = false;

		highCard = sortedCards[4].getValue();

		Map<Integer, Long> countedSuits = countSuits(sortedCards);
		if (countedSuits.containsValue(5l)) {
			isFlush = true;
		}
		for (int i = 0; i < 4; i++) {
			isStraight = true;
			if (sortedCards[i].getValue() + 1 != sortedCards[i + 1].getValue()) {
				isStraight = false;
				break;
			}
		}
		if (isFlush && isStraight) {
			handRank = HandRank.STRAIGHT_FLUSH;
			setHandValue(highCard);
			return handRank;
		}

		if (isFlush) {
			handRank = HandRank.FLUSH;
			setHandValue(highCard);
			return handRank;
		}

		if (isStraight) {
			handRank = HandRank.STRAIGHT;
			setHandValue(highCard);
			return handRank;
		}

		Map<Integer, Long> countedValues = countValues(sortedCards);
		for (Map.Entry<Integer, Long> countedValue : countedValues.entrySet()) {
			if (countedValue.getValue() == 4l) {
				handRank = HandRank.FOUR_OF_AKIND;
				setHandValue(countedValue.getKey());
				return handRank;
			}

			if (countedValue.getValue() == 3l) {
				isThree = true;
				highCard = countedValue.getKey();
			}

			if (countedValue.getValue() == 2l) {
				if (isPair == false) {
					isPair = true;
					if (isThree == false) {
						highCard = countedValue.getKey();
					}
				} else {
					isSecondPair = true;
					if (highCard < countedValue.getKey()) {
						highCard = countedValue.getKey();
					}
				}
			}
		}

		if (isThree && isPair) {
			handRank = HandRank.FULL_HOUSE;
			setHandValue(highCard);
			return handRank;
		}

		if (isThree) {
			handRank = HandRank.THREE_OF_A_KIND;
			setHandValue(highCard);
			return handRank;
		}

		if (isPair && isSecondPair) {
			handRank = HandRank.TWO_PAIRS;
			setHandValue(highCard);
			return handRank;
		}

		if (isPair) {
			handRank = HandRank.PAIR;
			setHandValue(highCard);
			return handRank;
		}

		handRank = HandRank.HIGH_CARD;
		setHandValue(highCard);
		return handRank;

	}

	public String compareTo(Hand secondHand) {
		String resultMessage = "";
		HandRank thisHandRank = this.rank();
		HandRank secondHandRank = secondHand.rank();
		resultMessage += FIRST_HAND + thisHandRank.getCause() + System.lineSeparator();
		resultMessage += SECOND_HAND + secondHandRank.getCause() + System.lineSeparator();

		if (thisHandRank.getRank() > secondHandRank.getRank()) {
			resultMessage += FIRST_HAND_WINS;
			return resultMessage;
		}
		
		if (thisHandRank.getRank() < secondHandRank.getRank()) {
			resultMessage += SECOND_HAND_WINS;
			return resultMessage;
		}
		
		if (thisHandRank.getRank() == secondHandRank.getRank()) {
			if (this.getHandValue() == secondHand.getHandValue()){
				resultMessage += DRAW;
				return resultMessage;
			}
			if(this.getHandValue() > secondHand.getHandValue()){
				resultMessage += FIRST_HAND_WINS;
				return resultMessage;
			}
			if(this.getHandValue() < secondHand.getHandValue());
				resultMessage += SECOND_HAND_WINS;
				return resultMessage;
			}
		return null;
		}
}
