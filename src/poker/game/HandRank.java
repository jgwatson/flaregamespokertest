package poker.game;

public enum HandRank {
	STRAIGHT_FLUSH("Straight flush",10),
	FOUR_OF_AKIND("Four of a kind",9),
	FULL_HOUSE("Full house",8),
	FLUSH("Flush",7),
	STRAIGHT("Straight",6),
	THREE_OF_A_KIND("Three of a Kind",5),
	TWO_PAIRS("Two pairs",4),
	PAIR("Pair",3),
	HIGH_CARD("High Card",2);
	
	private final String cause;
	private final int rank;
	
	HandRank(String cause, int rank){
		this.cause = cause;
		this.rank = rank;
	}
	
	public String getCause() {
		return cause;
	}
	
	public int getRank() {
		return rank;
	}
	
}

