package poker.game;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
	private ArrayList<Card> shuffledCards;
	private ArrayList<Card> unShuffledCards;

	public Deck(){
		unShuffledCards = createFullNonRandomizedDeck();
		shuffleCards();
	}

	public ArrayList<Card> getUnShuffledCards() {
		return unShuffledCards;
	}

	public ArrayList<Card> getShuffledCards() {
		return shuffledCards;
	}
	
	private ArrayList<Card> createFullNonRandomizedDeck(){
		ArrayList<Card> fullCards = new ArrayList<Card>();
		for (int suit = 0; suit < 4; suit++){
			for (int value = 0; value < 13; value++){
				fullCards.add(new Card(value, suit));
			}
		}
		return fullCards;
	}
	
	public void shuffleCards(){
		shuffleCards(100);
	}
	
	public void shuffleCards(int numberOfIterations){
		Random randomGenerator = new Random();
		ArrayList<Card> cardsToShuffle = new ArrayList<Card>(); 
		cardsToShuffle.addAll(unShuffledCards);
		int firstCardIndex;
		int secondCardIndex;
		
		for (int i = 0; i < numberOfIterations; i++){
			firstCardIndex = randomGenerator.nextInt(unShuffledCards.size()-1);
			secondCardIndex = randomGenerator.nextInt(unShuffledCards.size()-1);
			Card firstCard =  cardsToShuffle.get(firstCardIndex);
			Card secondCard = cardsToShuffle.get(secondCardIndex); 
			cardsToShuffle.set(secondCardIndex, firstCard);
			cardsToShuffle.set(firstCardIndex, secondCard);
		}
		shuffledCards = cardsToShuffle;
	}
	
	public Card drawACard(){
		return shuffledCards.remove(0);
	}
}
