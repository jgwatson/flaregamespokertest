package poker.game;

import java.util.Arrays;

public class Card {
	private int value;
	private int suit;
	public static String[] VALUES = { "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A" };
	public static String[] SUITS = { "C", "D", "H", "S" };

	public Card(int value, int suit) {
		if ((value < 0 || value > 12) || (suit < 0 || suit > 3)) {
			throw new RuntimeException("Invalid Card Arguments");
		}
		this.value = value;
		this.suit = suit;
	}

	public Card(String valueAndSuit) {
		if (valueAndSuit.length() != 2) {
			throw new RuntimeException("Invalid Card Arguments");
		}
		String valueString = valueAndSuit.substring(0, 1);
		String suitString = valueAndSuit.substring(1, 2);
		int value = Arrays.asList(VALUES).indexOf(valueString);
		int suit = Arrays.asList(SUITS).indexOf(suitString);
		if ((value < 0 || value > 12) || (suit < 0 || suit > 3)) {
			throw new RuntimeException("Invalid Card Arguments");
		}
		this.value = value;
		this.suit = suit;
	}
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) {
		this.suit = suit;
	}
	
	@Override
	public String toString() {
		String valueString = VALUES[value];
		String suitString = SUITS[suit];
		return valueString + suitString;
	}

	@Override
	public boolean equals(Object object) {
		if (object != null && object instanceof Card) {
			Card card = (Card) object;
			return value == card.value && suit == card.suit;
		}
		return false;
	}
}
